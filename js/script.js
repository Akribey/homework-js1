  // Theoretical question
  //   1. let and const are block scoped,
  //      const is limited by curly braces and can not be reassigned
  //      let can be;
  //   2. var is limited to the function within which it is defined
  //      If it is defined outside any function, the scope of the variable is global
  //      It makes insecure(bad tone) using var

const name  = prompt("Please, enter your name");
if(!isNaN(name)){
    alert("Don't use numbers, only letters");
}
else{
    const age = prompt ("Please, enter your age");
    if(isNaN(age)){
        alert("Enter numbers, not letters");
    }
    else {
        if (age < 18) {
            alert("You are not allowed to visit this website");
        } else if (age > 22) {
            alert("Welcome!");
        } else {
            a = confirm("Are you sure you want to continue?");
            if (a == true) {
                alert("Welcome");
            } else {
                alert("You are not allowed to visit this website");
            }
        }
    }

}

